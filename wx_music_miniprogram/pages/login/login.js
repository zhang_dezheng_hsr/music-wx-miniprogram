// pages/login/login.js
import request from '../../utils/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phoneReg: /^1(?:3|4|5|7|8|9)[0-9]{9}$/,
    phone: '',
    password: '',
    test: 'master'
  },
  inputNum (e) {
    this.setData({
      [e.target.id]: e.detail.value
    })
    console.log('***inputNum**e******', e.target.id)
    console.log('***inputPassword**e******', e.detail.value)
    // console.log('***inputNum**e******', e.target.dataset.id)
    // console.log('***inputPassword**password******', this.data.password)
    // console.log('***inputPhone**phone******', this.data.phone)
  },
  inputPassword (e) {
    // console.log('***inputPassword**e******', e.detail.value)
    this.setData({
      password: e.detail.value
    })
    console.log('***inputPassword**password******', this.data.password)
  },
  inputPhone (e) {
    // console.log('***inputPhone**phone******', e)
    this.setData({
      phone: e.detail.value
    })
    console.log('***inputPhone**phone******', this.data.phone)
  },
  async login () {
    // 验证手机号
    if (!/^1(3|4|5|6|7|8|9)[0-9]{9}$/.test(this.data.phone)) {
      return wx.showToast({
        title: '手机号格式错误！',
        icon: 'none'
      })
    }
    if (!this.data.password.trim()) {
      return wx.showToast({
        title: '密码不能为空！',
        icon: 'none'
      })
    }
    const res = await request({
      url: '/login/cellphone',
      data: {
        phone: this.data.phone,
        password: this.data.password
      }
    })
    console.log('****request********', res)
    res.cookies && wx.setStorageSync('cookie', res.cookies[0])
    // console.log('****request*statusCode*******', res.statusCode)
    console.log('****cookie*******', wx.getStorageSync('cookie'))
    // console.log('****cookies*******', wx.getStorageSync('cookies'))
    if (res.statusCode === 200) {
      wx.reLaunch({
        url: '/pages/personal/personal',
      })
      wx.setStorageSync('cookies', JSON.stringify(res.cookies))
      res.cookies && wx.setStorageSync('cookie', res.cookies[2])
      wx.setStorageSync('profile', JSON.stringify(res.data.profile))
      // let x = wx.getStorageSync('cookies')
      // x = JSON.parse(x)
      // console.log('********x*****', x)
      return wx.showToast({
        title: '登录成功！'
      })
    }
    switch (res.statusCode) {
      case 400:
        wx.showToast({
          title: '手机号错误',
          icon: 'none'
        });
        break;
      case 502:
        wx.showToast({
          title: '密码错误',
          icon: 'none'
        });
        break;
      default:
        wx.showToast({
          title: '登录失败，请重新登录',
          icon: 'none'
        });
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})