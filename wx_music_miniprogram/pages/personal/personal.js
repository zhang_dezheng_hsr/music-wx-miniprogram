// pages/personal/personal.js
import request from '../../utils/request'
let startY = 0
let moveY = 0
let distanceY = 0
Page({

  /**
   * 页面的初始数据
   */
  data: {
    infoTransform: '',
    infoTransition: '',
    profile: null,
    weekData: []
  },
  start: function (e) {
    startY = e.touches[0].clientY
    this.setData({
      infoTransform: `translateY(0rpx)`
    })
    console.log('******e******', e.touches[0].clientY)
    console.log('******startY******', startY)
  },
  move: function (e) {
    moveY = e.touches[0].clientY - startY
    distanceY = moveY
    if (moveY < 0 || moveY > 156) return
    this.setData({
      infoTransform: `translateY(${distanceY}rpx)`
    })
    console.log('******e******', e.touches[0].clientY)
    console.log('******moveY******', moveY)
  },
  end: function (e) {
    this.setData({
      infoTransform: `translateY(0rpx)`,
      // 这句话写的位置值得考究，如果在data里面，那么一开始就会生效； 
      // 写在这个位置，就是从这个位置开始生效。这个确实有点区别。
      // 这里的作用让y方向缩回变慢！！！
      infoTransition: 'all 1s linear'
    })
    console.log('******e******', e)
  },
  navLogin () {
    if (this.data.profile) {
      return wx.showToast({
        title: '您已登录！',
        icon: 'none'
      })
    }
    wx.navigateTo({
      url: '/pages/login/login',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let profile = JSON.parse(wx.getStorageSync('profile'))
    this.setData({
      profile
    })
    if (!profile) return
    console.log('profile', profile)
    let res = await request({
      url: '/user/record',
      data: {
        uid: profile.userId,
        type: 1
      }
    })
    let index = 0
    this.setData({
      weekData: res.data.weekData.map(i => {
        i.index = index++
        return i
      })
    })
    console.log("****res**profile******", res)
    for(let i of res.data.weekData) {
      console.log("****res**profile******", i.song.al.picUrl)
    }
    console.log("****res**weekData******", this.data.weekData)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})