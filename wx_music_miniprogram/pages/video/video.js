// pages/video/video.js
import request from '../../utils/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    videoTagsList: [],
    navId: '',
    tagList: [],
    // 当前播放的视频的id
    vid: '',
    // 存储播放的历史记录
    vList: [],
    // scroll-view是否被下拉刷新
    isRefresh: false
  },

  // 去搜索页
  toSearch () {
    wx.navigateTo({
      url: '/pages/search/search',
    })
  },

  async videoTagsList () {
    let res = await request({
      url: '/video/group/list'
    })
    let videoTagsList = res.data.data.slice(0, 20) || []
    console.log('**videoTagsList**', videoTagsList)
    this.setData({
      videoTagsList,
      navId: videoTagsList[0].id
    })
    this.getListOfNavId()
  },

  async getListOfNavId () {
    // let one = (JSON.parse(wx.getStorageSync('cookies'))).find(i => i.indexOf('MUSIC_U') !== -1)
    // let one = storageList.find(i => i.indexOf('MUSIC_U') !== -1)
    console.log('**navId**', this.data.navId)
    let res = await request({
      url: '/video/group',
      data: {
        id: this.data.navId
      }
      // header: {
      //   cookie: one || '',
      //   a: 'aaa'
      // }
    })
    console.log('/video/group', res)
    if (!res.data) return
    let index = 0
    let tagList = res.data.datas.map(i => {
      i.id = index++
      return i
    })
    console.log('**tagList**', tagList)
    this.setData({
      tagList,
      isRefresh: false
    })
    console.log('cookie', wx.getStorageSync('cookie'))
    // console.log('****cookies*******', JSON.parse(wx.getStorageSync('cookies')))
    // let storageList = JSON.parse(wx.getStorageSync('cookies'))
    // let one = storageList.find(i => i.indexOf('MUSIC_U'))
    // console.log('cookie********one', one)
  },

  // 点击菜单项
  async getNavId (e) {
    console.log('******', e)
    this.setData({
      navId: e.target.dataset.navId
    })
    await this.getListOfNavId()
  },

  // 开始播放
  // 总结
  // 为什么之前依次播放视频A、B、C、A时会有两个A在播放？
  // 因为你在video组件上去wx:if,又去拿video上的video上下文，这样后面拿到的上下文是新创建的video上下文，这样拿到的就是播放同一个视频url的不同video上下文。所以解决问题的关键是要保持某个url的viedo上下文是每次拿到的都是同一个viedo上下文
  videoPlay (e) {
    let vid = e.target.id
    console.log('vid', vid)
    // 将上一首歌暂停
    // this.data.vid !== vid 是考虑到上一首歌跟现在播放的是同一首歌
    // 如果是对video组件用wx:if,上一个视频组件必定被销毁，没必要在获取上一个video去暂停它
    // if (this.data.vid && this.data.vid !== vid) {
    //   let Selector = wx.createSelectorQuery(this.data.vid)
    //   console.log('dom-vid-video', Selector)
    //   // let vContext = wx.createVideoContext(this.data.vid)
    //   let vContext = wx.createVideoContext(this.data.vid, Selector)
    //   vContext.stop()
    // }
    // 2021-12-6，注释上面代码，对video使用wx：if，这时候好像又把问题解决了，但是保存时间的记录好像不起作用了。
    // let { vList } = this.data
    // let findItem = vList.find(i => i.vid === vid)
    // let vContext
    // if (findItem && findItem.context) vContext = findItem.context
    // else if (findItem && !findItem.context) {
    //   let Selector = wx.createSelectorQuery(vid)
    //   vContext = wx.createVideoContext(vid, Selector)
    //   findItem.context = vContext
    // }
    // if (findItem) {
    //   if (!findItem.context) {
    //     let Selector = wx.createSelectorQuery(vid)
    //     vContext = wx.createVideoContext(vid, Selector)
    //     findItem.context = vContext
    //     return
    //   } else {
    //     vContext = findItem.context
    //   }
    //   // vContext = findItem.context
    //   let currentTime = findItem.currentTime
    //   // 测试下下面这行没看出明显作用
    //   vContext.seek(currentTime)
    // } else {
    //   let Selector = wx.createSelectorQuery(vid)
    //   vContext = wx.createVideoContext(vid, Selector)
    //   vList.push({
    //     vid,
    //     context: vContext
    //   })
    // }
    let Selector = wx.createSelectorQuery(vid)
    let vContext = wx.createVideoContext(vid, Selector)
    this.setData({
      vid
    })
    setTimeout(() => {
      vContext.play()
    }, 500)
    // vContext.play()
  },
  // 监听播放进度
  videoTimeupdate (e) {
    // console.log('*****videoTimeupdate****', e.detail.currentTime)
    let vid = e.target.id
    let currentTime = e.detail.currentTime
    let { vList } = this.data
    let findItem = vList.find(i => i.vid === vid)
    if (findItem) {
      findItem.currentTime = currentTime
    } else {
      vList.push({
        vid,
        currentTime
      })
    }
    this.setData({
      vList
    })
    // console.log('this.data.vList', this.data.vList)
  },
  // 自然播放结束
  videoEnded (e) {
    let vid = e.target.id
    // console.log('vid', vid)
    let { vList } = this.data
    let Index = vList.findIndex(i => i.vid === vid)
    let Selector = wx.createSelectorQuery(vid)
    let vContext = wx.createVideoContext(vid, Selector)
    if(Index !== -1) vList.splice(Index, 1)
    vContext.stop()
  },
  // 暂停播放
  videoPause (e) {
    // console.log('*******pause*********')
    let vid = e.target.id
    let Selector = wx.createSelectorQuery(vid)
    let vContext = wx.createVideoContext(vid, Selector)
    // if(Index !== -1) vList.splice(Index, 1)
    vContext.pause()
  },

  refreshScrollView (e) {
    this.setData({
      isRefresh: true
    })
    this.getListOfNavId()
    // this.setData({
    //   isRefresh: false
    // })
    console.log('**refreshScrollView***')
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.videoTagsList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})