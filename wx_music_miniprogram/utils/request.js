import config from './config'
export default ({
  url,
  data = {},
  method = 'GET'
  // header
}) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: config.host + url,
      data,
      // dataType: dataType,
      // header: header,
      method,
      // header,
      header: {
        // wx.getStorageSync('cookies')得有值，JSON.parse才能作用于它，否则会报错，类似于JSON.parse（undefined）
        cookie: (wx.getStorageSync('cookies') && (JSON.parse(wx.getStorageSync('cookies'))).find(i => i.indexOf('MUSIC_U') !== -1)) || ''
      },
      success: (result) => {
        resolve(result)
      },
      fail: (res) => {
        reject(res)
      }
    })
  })
}


